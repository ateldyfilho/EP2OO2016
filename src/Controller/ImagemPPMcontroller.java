package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import Model.ImagemPPM;
import View.ImagemView;

public class ImagemPPMcontroller extends ImagemController {
	protected ImagemPPM modelPPM;
	private ActionListener actionListenerFiltroVermelho;
	private ActionListener actionListenerFiltroVerde;
	private ActionListener actionListenerFiltroAzul;
	private ActionListener actionListenerFiltroNegativo;

	public ImagemPPMcontroller(ImagemPPM model, ImagemView view) {
		super(model, view);
		// TODO Auto-generated constructor stub
	}
	
	public void onClick(final JFrame frame){
		actionListenerFiltroVermelho = new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				view.mostrarImagem(ImageFiltroVermelho(LerImagem()));
				//System.out.println("");
			}
		};
		view.getFiltroVermelhoButton().addActionListener(actionListenerFiltroVermelho); 
		
		actionListenerFiltroVerde = new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				view.mostrarImagem(ImageFiltroVerde(LerImagem()));
			}
		};
		view.getFiltroVerdeButton().addActionListener(actionListenerFiltroVerde);
		
		actionListenerFiltroAzul = new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				view.mostrarImagem(ImageFiltroAzul(LerImagem()));
			}
		};
		view.getFiltroAzulButton().addActionListener(actionListenerFiltroAzul);
		
		actionListenerFiltroNegativo = new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				view.mostrarImagem(ImageFiltroNegativo(LerImagem()));
			}
		};
		view.getFiltroNegativoButton().addActionListener(actionListenerFiltroNegativo);
	}
	
	public String MensagemImagem() {
		if(model.getTipoImagem().equals("P6")){
			modelPPM.setCorMensagem(model.getMensagem());
		}
		return modelPPM.getCorMensagem();
	}
	
	public String ImageFiltroVermelho(BufferedImage image){
		
		
		String imagemTrajeto = new String("");
		
		image.createGraphics().drawRenderedImage(image, null);
		try {
			ImageIO.write(image,"jpg",new File(System.getProperty("java.io.tmpdir") + "/outputRed.jpg"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int width = model.getTamImagem().get(0);
		int height = model.getTamImagem().get(1);
			
		for(int y = 0; y < height;y++) {
			for(int x = 0; x < width; x++) {
				int pixel = image.getRGB(x,y);
				
				int alpha = (pixel>>24)&0xff;
				int red = (pixel>>16)&0xff;
				
				pixel = (alpha<<24) | (red<<16) | (0<<8) | 0;
				
				image.setRGB(x,y,pixel);
				
			}	
		}
		try {
			imagemTrajeto = new String("outputRed.jpg");
			File f = new File(imagemTrajeto);
			ImageIO.write(image,"jpg", f);			
		}catch(IOException e) {
			System.out.println(e);
		}
		
		return imagemTrajeto;
	}
	
	public String ImageFiltroVerde(BufferedImage image){
		String imagemTrajeto = new String("");

		image.createGraphics().drawRenderedImage(image, null);
		try {
			ImageIO.write(image,"jpg",new File(System.getProperty("java.io.tmpdir") + "/outputGreen.jpg"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int width = model.getTamImagem().get(0);
		int height = model.getTamImagem().get(1);
		
		for(int y = 0; y < height;y++) {
			for(int x = 0; x < width; x++) {
				int pixel = image.getRGB(x,y);
				
				int alpha = (pixel>>24)&0xff;
				int green = (pixel>>8)&0xff;
				
				pixel = (alpha<<24) | (0<<16) | (green<<8) | 0;
				
				image.setRGB(x,y,pixel);
			}
		}
		try {
			imagemTrajeto = new String("outputGreen.jpg");
			File f = new File(imagemTrajeto);
			ImageIO.write(image,"jpg", f);			
		}catch(IOException e) {
			System.out.println(e);
		}
		
		return imagemTrajeto;
	}
	
	public String ImageFiltroAzul(BufferedImage image){
		String imagemTrajeto = new String("");
		
		image.createGraphics().drawRenderedImage(image, null);
		try {
			ImageIO.write(image,"jpg",new File(System.getProperty("java.io.tmpdir") + "/outputBlue.jpg"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int width = model.getTamImagem().get(0);
		int height = model.getTamImagem().get(1);
		
		for(int y = 0; y < height;y++) {
			for(int x = 0; x < width; x++) {
				int pixel = image.getRGB(x,y);
				
				int alpha = (pixel>>24)&0xff;
				int blue = (pixel)&0xff;
				
				pixel = (alpha<<24) | (0<<16) | (0<<8) | blue;
				
				image.setRGB(x,y,pixel);
			}
		}
		try {
			imagemTrajeto = new String("outputBlue.jpg");
			File f = new File(imagemTrajeto);
			ImageIO.write(image,"jpg", f);			
		}catch(IOException e) {
			System.out.println(e);
		}
		
		return imagemTrajeto;
	}
	
	public String ImageFiltroNegativo(BufferedImage image) {
		String imagemTrajeto = new String("");
		
		image.createGraphics().drawRenderedImage(image, null);
		try {
			ImageIO.write(image,"jpg",new File(System.getProperty("java.io.tmpdir") + "/outputNegative.jpg"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int width = model.getTamImagem().get(0);
		int height = model.getTamImagem().get(1);
		
		for(int y = 0; y < height;y++) {
			for(int x = 0; x < width; x++) {
				int pixel = image.getRGB(x,y);
				
				pixel = model.getValuePixel() - pixel;
				
				image.setRGB(x,y,pixel);
			}
		}
		try {
			imagemTrajeto = new String("/home/ateldy/Desktop/output4.jpg");
			File f = new File(imagemTrajeto);
			ImageIO.write(image,"jpg", f);			
		}catch(IOException e) {
			System.out.println(e);
		}
		
		return imagemTrajeto;
	}
}

