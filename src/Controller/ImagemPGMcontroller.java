package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import Model.ImagemPGM;
import Model.ImagemPPM;
import View.ImagemView;

public class ImagemPGMcontroller extends ImagemController {

	protected ImagemPGM modelPGM;
	private ActionListener actionListenerFiltroSharpen;
	private ActionListener actionListenerFiltroSmooth;
	private ActionListener actionListenerShowSecret;
	
	public ImagemPGMcontroller(ImagemPPM model, ImagemView view) {
		super(model, view);
		// TODO Auto-generated constructor stub
	}
	
	public void onClick(final JFrame frame){
		actionListenerFiltroSharpen = new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				view.mostrarImagem(ImageFiltroSharpen(LerImagem()));
			}
		};
		view.getFiltroSharpenButton().addActionListener(actionListenerFiltroSharpen);
		
		actionListenerFiltroSmooth = new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				view.mostrarImagem(ImageFiltroSmooth(LerImagem()));
			}
		};
		view.getFiltroSmoothButton().addActionListener(actionListenerFiltroSmooth);
		
		actionListenerShowSecret = new ActionListener(){


			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//view.mostrarImagem(ImageFiltroSmooth(LerImagem()));
			}
		};
		view.getMostrarSegredoButton().addActionListener(actionListenerShowSecret);
	} 
		
	public String ImageFiltroSharpen(BufferedImage image){
		String imagemTrajeto = new String("");
		
		image.createGraphics().drawRenderedImage(image,null);
		try {
			ImageIO.write(image,"jpg",new File(System.getProperty("java.io.tmpdir") + "/outputSharpen.jpg"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Kernel kernel = new Kernel(3,3, new float[] {0, -1, 0, -1, 5, -1, 0, -1, 0 });
		
		BufferedImageOp op = new ConvolveOp(kernel);
		image = op.filter(image, null);

		try {
			imagemTrajeto = new String("outputSharpen.jpg");
			File f = new File(imagemTrajeto);
			ImageIO.write(image,"jpg", f);		
		}catch(IOException e) {
			System.out.println(e);
		}
		
		return imagemTrajeto;
	}
	
	public String ImageFiltroSmooth(BufferedImage image){
		String imagemTrajeto = new String("");
		
		image.createGraphics().drawRenderedImage(image,null);
		try {
			ImageIO.write(image,"jpg",new File(System.getProperty("java.io.tmpdir") + "/outputSmooth.jpg"));
			} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Kernel kernel = new Kernel(3,3, new float[] {1.0f/9, 1.0f/9, 1.0f/9, 1.0f/9, 1.0f/9, 1.0f/9, 1.0f/9, 1.0f/9, 1.0f/9 });
		BufferedImageOp op = new ConvolveOp(kernel);
		image = op.filter(image, null);

		try {
			imagemTrajeto = new String("outputSmooth.jpg");
			File f = new File(imagemTrajeto);
			ImageIO.write(image,"jpg", f);		
		}catch(IOException e) {
			System.out.println(e);
		}
		
		return imagemTrajeto;
	}
	
}
