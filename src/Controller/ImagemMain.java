package Controller;

/**
 * 
 * @author ateldy
 *
 */

import Model.ImagemPPM;
import View.ImagemView;

public class ImagemMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
					
			ImagemView view = new ImagemView();
			ImagemPPM model = new ImagemPPM();
			ImagemController controller = new ImagemController(model, view);
			ImagemPPMcontroller controllerPPM = new ImagemPPMcontroller(model,view);
			ImagemPGMcontroller controllerPGM = new ImagemPGMcontroller(model,view);
			
			view.screen();
			controller.onClick(view.frame);
			controller.updateView();
			controllerPPM.onClick(view.frame);
			controller.updateView();
			controllerPGM.onClick(view.frame);
			controller.updateView();
	}

}
