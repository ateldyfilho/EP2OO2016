package Controller;

/**
 * 
 * @author ateldy
 *
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import Model.AbstractImage;
import View.ImagemView;

public class ImagemController {
	protected AbstractImage model;
	protected ImagemView view;
	private ActionListener actionListener;
	private static JFileChooser fileChooser;
	
	public ImagemController(AbstractImage model, ImagemView view){
		this.model = model;
		this.view = view;
	}
	
	public void onClick(final JFrame frame){
		actionListener = new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				chooseFile(frame);
			}
		};
		view.getSearchButton().addActionListener(actionListener);
	}
	
	public void chooseFile(JFrame frame) {
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = fileChooser.showOpenDialog(frame);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = fileChooser.getSelectedFile();
		    model.setTrajetoImagem(selectedFile.getAbsolutePath());
		   
		    
		    ReadHeaderImage();
		    LerImagem();
		}
	}
	

	public void ReadHeaderImage() {
		File imageFile = new File(model.getTrajetoImagem());
		
		try {
			model.setimagemHeadLeitor(new BufferedReader(new FileReader(imageFile)));
			List<Integer> tamImagem = new ArrayList<Integer>();
			
			model.setTipoImagem(model.getimagemHeadLeitor().readLine());
			model.setMensagem(model.getimagemHeadLeitor().readLine());
			String[] firstLine = model.getimagemHeadLeitor().readLine().split(" ");
			tamImagem.add(Integer.parseInt(firstLine[0]));
			tamImagem.add(Integer.parseInt(firstLine[1]));
			model.setTamImagem(tamImagem);
			model.setValuePixel(Integer.parseInt(model.getimagemHeadLeitor().readLine()));
	        	
			updateView();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public BufferedImage LerImagem() {		
			BufferedImage image = null;
			try {
				image = ImageIO.read(new File(model.getTrajetoImagem()));
				model.setImagemLeitor(image);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return image;
	}
	
	public void updateView(){
		view.printImageDetails(model.getTipoImagem(),model.getTrajetoImagem(), model.getTamImagem(), model.getValuePixel(), model.getMensagem());
		view.mostrarImagem(model.getTrajetoImagem());
	}
	
	public String getTrajetoImagem() {
		return model.getTrajetoImagem();
	}
	
	public void setTrajetoImagem(String TrajetoImagem) {
		model.setTrajetoImagem(TrajetoImagem);
	}
	
	public List<Integer> getTamImagem() {
		return model.getTamImagem();
	}
	
	public void setTamImagem(List<Integer> tamImagem) {
		model.setTamImagem(tamImagem);
	}
	
	public int getValuePixel() {
		return model.getValuePixel();
	}
	
	public void setValuePixel(int valuePixel) {
		model.setValuePixel(valuePixel);
	}
	
	public BufferedReader getimagemHeadLeitor() {
		return model.getimagemHeadLeitor();
	}
	
	public void setImagemHeadLeitor(BufferedReader imagemHeadLeitor) {
		model.setimagemHeadLeitor(imagemHeadLeitor);
	}
	
	public void setImagemLeitor(BufferedImage imagemLeitor) {
		model.setImagemLeitor(imagemLeitor);
	}

}