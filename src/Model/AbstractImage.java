package Model;

/**
 * 
 * @author ateldy
 *
 */

import java.util.List;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;

public abstract class AbstractImage {
	protected String trajetoImagem;
	protected List<Integer> tamImagem;
	protected int valuePixel;
	protected String mensagem;
	protected String tipoImagem;
	protected BufferedReader imagemHeadLeitor;
	protected BufferedImage imagemLeitor;
	
	
	public String getTrajetoImagem() {
		return trajetoImagem;
	}
	
	public List<Integer> getTamImagem() {
		return tamImagem;
	}
	public void setTamImagem(List<Integer> tamImagem) {
		this.tamImagem = tamImagem;
	}
	public void setTrajetoImagem(String trajetoImagem) {
		this.trajetoImagem = trajetoImagem;
	}
	
	public int getValuePixel() {
		return valuePixel;
	}
	
	public void setValuePixel(int valuePixel) {
		this.valuePixel = valuePixel;
	}
	
	public BufferedReader getimagemHeadLeitor() {
		return imagemHeadLeitor;
	}
	
	public void setimagemHeadLeitor(BufferedReader imagemHeadLeitor) {
		this.imagemHeadLeitor = imagemHeadLeitor;
	}

	public BufferedImage getimagemLeitor() {
		return imagemLeitor;
	}

	public void setImagemLeitor(BufferedImage imagemLeitor) {
		this.imagemLeitor = imagemLeitor;
	}
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getTipoImagem() {
		return tipoImagem;
	}

	public void setTipoImagem(String tipoImagem) {
		this.tipoImagem = tipoImagem;
	}
}
