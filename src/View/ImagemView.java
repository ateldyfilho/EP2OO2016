package View;

/**
 * 
 * @author ateldy
 *
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class ImagemView {
	
	public JFrame frame;
	private JPanel mainPanel;
	private JLabel mainLabel;
	private JButton searchButton;
	private JButton filtroVermelho;
	private JButton filtroVerde;
	private JButton filtroAzul;
	private JButton filtroNegativo;
	private JButton filtroSharpen;
	private JButton filtroSmooth;
	private JButton MostrarSegredo;
	
	public void screen() {
		// TODO Auto-generated method stub
		frame = new JFrame();
		JFrame mainFrame = new JFrame("Filtros");
		mainFrame.setLayout(new BorderLayout());
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(700,500);
		
		mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(100,50));
		mainPanel.setBackground(Color.decode("#068B8B"));

		mainLabel = new JLabel("", JLabel.CENTER);
		mainLabel.setPreferredSize(new Dimension(900,600));
		mainPanel.add(mainLabel);
		
		JPanel headerPanel = new JPanel();
		headerPanel.setPreferredSize(new Dimension(700,50));
		
		JLabel headerLabel = new JLabel("Filtros", JLabel.CENTER);
		headerLabel.setFont(new Font("Serif", Font.BOLD, 36));
		headerLabel.setForeground(Color.white);
		headerPanel.add(headerLabel);
		
		JPanel rightPanel = new JPanel();
		rightPanel.setPreferredSize(new Dimension(180,50));
		rightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		rightPanel.setBackground(Color.decode("#068B8B"));
		
		JLabel rightLabel = new JLabel("Escolha uma ferrmenta  ", JLabel.RIGHT);
		rightLabel.setFont(new Font("Serif", Font.BOLD, 12));
		rightLabel.setForeground(Color.white);
		rightPanel.add(rightLabel);
		
		Icon search = new ImageIcon("res/searchButton.png");
		searchButton = new JButton("Search file", search);
		searchButton.setHorizontalTextPosition(SwingConstants.LEFT);
		searchButton.setPreferredSize(new Dimension(150, 30));
		searchButton.setBorder(null);
		searchButton.setForeground(Color.decode("#ADD8E6"));
		searchButton.setBackground(Color.decode("#008B8B"));
		rightPanel.add(searchButton);
		
		filtroVermelho = new JButton("Filtro Vermelho");
		filtroVermelho.setPreferredSize(new Dimension(150,30));
		filtroVermelho.setForeground(Color.decode("#ADD8E6"));
		filtroVermelho.setBackground(Color.decode("#008B8B"));
		rightPanel.add(filtroVermelho);
		
		filtroVerde = new JButton("Filtro Verde");
		filtroVerde.setPreferredSize(new Dimension(150,30));
		filtroVerde.setForeground(Color.decode("#ADD8E6"));
		filtroVerde.setBackground(Color.decode("#008B8B"));
		rightPanel.add(filtroVerde);
		
		filtroAzul = new JButton("Filtro Azul");
		filtroAzul.setPreferredSize(new Dimension(150,30));
		filtroAzul.setForeground(Color.decode("#ADD8E6"));
		filtroAzul.setBackground(Color.decode("#008B8B"));
		rightPanel.add(filtroAzul);
		
		filtroNegativo = new JButton("Filtro Negativo");
		filtroNegativo.setPreferredSize(new Dimension(150,30));
		filtroNegativo.setForeground(Color.decode("#ADD8E6"));
		filtroNegativo.setBackground(Color.decode("#008B8B"));
		rightPanel.add(filtroNegativo);
		
		filtroSharpen = new JButton("Filtro Sharpen");
		filtroSharpen.setPreferredSize(new Dimension(150,30));
		filtroSharpen.setForeground(Color.decode("#ADD8E6"));
		filtroSharpen.setBackground(Color.decode("#008B8B"));
		rightPanel.add(filtroSharpen);
		
		filtroSmooth = new JButton("Filtro Smooth");
		filtroSmooth.setPreferredSize(new Dimension(150,30));
		filtroSmooth.setForeground(Color.decode("#ADD8E6"));
		filtroSmooth.setBackground(Color.decode("#008B8B"));
		rightPanel.add(filtroSmooth);
		
		MostrarSegredo = new JButton("Segredo");
		MostrarSegredo.setPreferredSize(new Dimension(150,30));
		MostrarSegredo.setForeground(Color.decode("#ADD8E6"));
		MostrarSegredo.setBackground(Color.decode("#008B8B"));
		rightPanel.add(MostrarSegredo);
		
		mainFrame.add(mainPanel, BorderLayout.CENTER);
		mainFrame.add(headerPanel,BorderLayout.NORTH);
		mainFrame.add(rightPanel,BorderLayout.EAST);
		mainFrame.setVisible(true);
	}
	
	public JButton getSearchButton(){
        return searchButton;
    }
	
	public JButton getFiltroVermelhoButton() {
		return filtroVermelho;
	}
	
	public JButton getFiltroVerdeButton() {
		return filtroVerde;
	}
	
	public JButton getFiltroAzulButton() {
		return filtroAzul;
	}
	
	public JButton getFiltroNegativoButton() {
		return filtroNegativo;
	}
	
	public JButton getFiltroSharpenButton() {
		return filtroSharpen;
	}
	
	public JButton getFiltroSmoothButton() {
		return filtroSmooth;
	}
	public JButton getMostrarSegredoButton() {
		return MostrarSegredo;
	}
	
	public void printImageDetails(String tipoImagem, String trajetoImagem, List<Integer> tamImagem, int valuePixel, String Mensagem) {
		System.out.println("Tipo: " + tipoImagem);
		System.out.println("Trajeto: " + trajetoImagem);
		System.out.println("Tamanho Imagem: " + tamImagem);
		System.out.println("Value Pixel: " + valuePixel);
		System.out.println("Mensagem: " + Mensagem);
	}
	
	public int mostrarImagem(String trajetoImagem) {
		if(trajetoImagem != null) {
			ImageIcon imagemRedimensionada = new ImageIcon(trajetoImagem);
			imagemRedimensionada.getImage().flush();
			mainLabel.setIcon(imagemRedimensionada);
			return 1;
	} else {
		return 0;
		}
	}
}
