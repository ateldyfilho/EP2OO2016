# EP 2 - Orientação à Objetos
##### Ateldy Borges Brasil Filho 15/0006101

# Executando

O programa abrirá uma janela , clique no botão "search file" e selecione a imagem. 
Selecione o filtro desejado e ele será projetado na tela.

Use a opção "segredo" para decifrar mensagens em PGM.

Para decifrar as imagens PPM o programa indicará na janela qual filtro deve ser utilizado.

A aplicação decifra apenas imagens PPM e PGM e salvará em formato JPG.